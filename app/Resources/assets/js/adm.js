/**
 * Created by Justin on 12/12/2016.
 */

$(document).ready(function () {
	try {
		$("#usersEnabled")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});

		$("#usersNotEnabled")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});

		$("#usersNotRegistered")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});
	}
	catch (e) {
		// console.error(e);
	}

	try {
		$("#customersEnabled")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});

		$("#customersNotEnabled")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});
	}
	catch (e) {
		// console.error(e);
	}

	try {
		$("#systemsEnabledList")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});
		$("#systemsNotEnabledList")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});
	}
	catch (e) {
		// console.error(e);
	}

	$('.clickable-row').click(function () {
		window.location = $(this).data("href")
	});

	$('.number-input').on('change', function(){
		$(this).val(parseFloat($(this).val()).toFixed(1));
	});

	$("#systems_new_edit_form_systemId").click(function () {
		console.log("VAL: " + $(".js-regesteredFind").val());
	});


});

