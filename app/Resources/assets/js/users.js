
$(document).ready(function () {

	/*
	 * Used to sort the Admin/Users Roles select
	 */
	$(".js-usersRolesChoicesSelect").html($(".js-usersRolesChoicesSelect option").sort(function (a, b) {
		return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
	}));
	var sa = $(".js-usersRolesChoicesSelect").find('option[value="ROLE_USER"]');
	sa.remove();
	$(".js-usersRolesChoicesSelect").find('option:eq(0)').before(sa);

	if($("#editState").text() != ''){
		$(".js-usersStateSelectFind").val($("#editState").text());
	}

});