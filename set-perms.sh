#!/usr/bin/env bash
cd /var/www/
chown -R www-data.www-data oihealthcare_com
cd oihealthcare_com
chmod 770 *.sh
chmod -R g+w /var/www/oihealthcare_com/var