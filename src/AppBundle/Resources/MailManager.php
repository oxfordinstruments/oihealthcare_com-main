<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 11/16/2017
 * Time: 10:10 AM
 */

namespace AppBundle\Resources;


use Swift_Message;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Template;


class MailManager
{
	/**
	 * @var ContainerInterface
	 */
	private $container;

	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}


	/**
	 * @param string $email
	 * @param string $subject
	 * @param array $parameters
	 * @param Template|string $template
	 * @return int
	 */
	public function sendEmail(string $email, string $subject, array $parameters, $template)
	{
		$this->container->get('monolog.logger.oivision')
			->info('Sending Email. TO: '.$email."  SUBJECT: ".$subject,
				['key' => 'app', 'function' => __CLASS__.'::'.__FUNCTION__]);

		$message = (new Swift_Message($subject))
			->setTo($email)
			->setFrom([$this->container->getParameter('mailer_sender_address') => $this->container->getParameter('mailer_sender_name')])
			->setBody(
				$this->container->get('templating')->render(
					$template,
					$parameters
				),
				'text/html'
			);
		$mailer = $this->container->get('swiftmailer.mailer');

		$sent =  $mailer->send($message);
		if($sent > 0){
			$this->container->get('monolog.logger.oivision')
				->info('Email Sent. TO: '.$email."  SUBJECT: ".$subject,
					['key' => 'app', 'function' => __CLASS__.'::'.__FUNCTION__]);
		}else{
			$this->container->get('monolog.logger.oivision')
				->error('Failed Email Send. TO: '.$email."  SUBJECT: ".$subject,
					['key' => 'app', 'function' => __CLASS__.'::'.__FUNCTION__]);
		}
		return $sent;
	}

}