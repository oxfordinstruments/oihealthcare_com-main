<?php

namespace AppBundle\Controller;


use AppBundle\Entity\SystemsDataGeEntity;
use AppBundle\Entity\SystemsEntity;
use AppBundle\Entity\TestEntity;
use AppBundle\Entity\UserEntity;
use AppBundle\Form\TestFormType;
use AppBundle\Resources\UtilitiesCommon;
use Detection\MobileDetect;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Zend\Json\Expr;
use \Indaxia\OTR\ITransformable;
use \Indaxia\OTR\Traits\Transformable;
use \Indaxia\OTR\Annotations\Policy;

class DefaultController extends Controller
{


	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction(Request $request)
	{
//		if (!is_null($this->getUser())) {
//			return $this->redirectToRoute('dashboard');
//		}

		return $this->render('Default/home.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
		]);
	}

	/**
	 * @Route("/none", name="under_construction")
	 */
	public function underConstructionAction(Request $request)
	{
		return $this->render('Default/none.html.twig');
	}

	/**
	 * @Route("/temp", name="temp")
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function temp(Request $request)
	{
		/**
		 * @var UserEntity $user
		 */
		$user = $this->container->get('doctrine')->getRepository(UserEntity::class)->findOneBy(['uid'=>'admin']);
		$utils = $this->container->get('app.utils_common');
		$roles = $utils->getInheritedRoles($user->getRoles());
		$user->setRoles($roles);

		$sysDataRepo = $this->container->get('doctrine')->getRepository(SystemsDataGeEntity::class);
		/**
		 * @var SystemsDataGeEntity $sysData
		 */
		$sysData = $sysDataRepo->getSystemsNotProcessedCritical();

		return $this->render('Emails/criticalAlarm.html.twig',
			[
				'name' => "Justin Davis",
				'data' => $sysData
			]
		);
	}
}

;