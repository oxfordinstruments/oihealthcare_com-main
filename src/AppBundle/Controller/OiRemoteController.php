<?php

namespace AppBundle\Controller;

use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class OiRemoteController extends Controller
{
	private $logger;
	/**
	 * OiRemoteController constructor.
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}

	public function indexAction($name)
	{
		return $this->render('', array('name' => $name));
	}

	/**
	 *
	 * This funct is here just for testing the OiRemote notify_server script it needs to be completed
	 *
	 * @Route("/remote/update", name="test_update")
	 */
	/*todo-evo Complete this function fo oiremote*/
	public function test(Request $request)
	{
		$odata = [
			'code' => 202,
			'error' => false,
		];

		$idata = false;
		try{
			$idata = json_decode($request->getContent());
			if(is_null($idata)){
				throw new Exception('Data is null');
			}
			$odata['idata'] = $idata;
		}
		catch (Exception $e){
			$odata['code'] = 400;
			$odata['error'] = $e->getMessage();
		}

		$this->logger->info("Notify Server: ".json_encode($odata));
		return new JsonResponse($odata, $odata['code']);
	}
}
