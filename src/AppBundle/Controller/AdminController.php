<?php

namespace AppBundle\Controller;


use AppBundle\Entity\SystemsDataGeEntity;

use AppBundle\Entity\SystemsEntity;
use AppBundle\Resources\combineOldData;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/admin", name="adminHomepage")
	 */
    public function indexAction()
    {
        return $this->render('Admin/admin.html.twig', array());
    }
}
