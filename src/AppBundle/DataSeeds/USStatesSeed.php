<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/29/2017
 * Time: 11:32 AM
 * Comments: https://github.com/soyuka/SeedBundle
 */

namespace AppBundle\DataSeeds;

use AppBundle\Entity\StatesEntity;
use Soyuka\SeedBundle\Command\Seed;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class USStatesSeed extends Seed
{
	protected function configure()
	{
		//The seed won't load if this is not set
		//The resulting command will be {prefix}:country
		$this
			->setSeedName('us-states');

		parent::configure();
	}

	public function load(InputInterface $input, OutputInterface $output){

		//Doctrine logging eats a lot of memory, this is a wrapper to disable logging
		$this->disableDoctrineLogging();

		//Access doctrine through $this->doctrine
		$statesRepository = $this->doctrine->getRepository('AppBundle:StatesEntity');


		foreach ($this->getStates() as $abv => $name) {

			if($statesRepository->findOneBy(array('abv' => $abv))) {
				continue;
			}

			$em = new StatesEntity();

			$em->setAbv($abv);
			$em->setName($name);

			//Doctrine manager is also available
			$this->manager->persist($em);

			$this->manager->flush();
		}

		$this->manager->clear();
	}

	/**
	 * Unload a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	public function unload(InputInterface $input, OutputInterface $output)
	{
		$em = $this->container->get('doctrine')->getEntityManager();
		$className = $em->getClassMetadata(StatesEntity::class)->getName();
		$em->createQuery('DELETE FROM '.$className)->execute();
	}

//	/**
//	 * @return int
//	 */
//	public function getOrder() {
//		return 0;
//	}

	public function getStates()
	{
		$states = array (
			'AL'=>'Alabama',
			'AK'=>'Alaska',
			'AZ'=>'Arizona',
			'AR'=>'Arkansas',
			'CA'=>'California',
			'CO'=>'Colorado',
			'CT'=>'Connecticut',
			'DE'=>'Delaware',
			'DC'=>'District of Columbia',
			'FL'=>'Florida',
			'GA'=>'Georgia',
			'HI'=>'Hawaii',
			'ID'=>'Idaho',
			'IL'=>'Illinois',
			'IN'=>'Indiana',
			'IA'=>'Iowa',
			'KS'=>'Kansas',
			'KY'=>'Kentucky',
			'LA'=>'Louisiana',
			'ME'=>'Maine',
			'MD'=>'Maryland',
			'MA'=>'Massachusetts',
			'MI'=>'Michigan',
			'MN'=>'Minnesota',
			'MS'=>'Mississippi',
			'MO'=>'Missouri',
			'MT'=>'Montana',
			'NE'=>'Nebraska',
			'NV'=>'Nevada',
			'NH'=>'New Hampshire',
			'NJ'=>'New Jersey',
			'NM'=>'New Mexico',
			'NY'=>'New York',
			'NC'=>'North Carolina',
			'ND'=>'North Dakota',
			'OH'=>'Ohio',
			'OK'=>'Oklahoma',
			'OR'=>'Oregon',
			'PA'=>'Pennsylvania',
			'RI'=>'Rhode Island',
			'SC'=>'South Carolina',
			'SD'=>'South Dakota',
			'TN'=>'Tennessee',
			'TX'=>'Texas',
			'UT'=>'Utah',
			'VT'=>'Vermont',
			'VA'=>'Virginia',
			'WA'=>'Washington',
			'WV'=>'West Virginia',
			'WI'=>'Wisconsin',
			'WY'=>'Wyoming',
		);
		return $states;
	}


}