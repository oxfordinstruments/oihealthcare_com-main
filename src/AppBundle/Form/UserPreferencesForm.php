<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 12:02 PM
 */

namespace AppBundle\Form;

use AppBundle\Entity\StatesEntity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class UserPreferencesForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder

			->add('emailAlarmNotify', ChoiceType::class, [
				'label' => 'Daily Alarms Notifications',
				'choices' => array('Yes' => true, 'No' => false),
			])

			->add('emailCriticalNotify', ChoiceType::class, [
				'label' => 'Critical Alarm Notifications',
				'choices' => array('Yes' => true, 'No' => false),
			])
			->add('savePreferences', SubmitType::class)
		;

	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'AppBundle\Entity\UserEntity',
		]);
	}

}