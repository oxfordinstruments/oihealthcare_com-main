<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/7/2017
 * Time: 11:33 AM
 */

namespace AppBundle\Form;


use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\TextType;
use Moment\Moment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotEqualTo;

class UserChangePasswordForm extends AbstractType
{
	protected $parameters_yml;

	public function __construct($parameters_yml)
	{
		$this->parameters_yml = $parameters_yml;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$now = new Moment('now');
		$builder
			->add('plainPassword', RepeatedType::class, [
				'type' => PasswordType::class,
				'invalid_message' => 'Passwords do not match',
				'required' => true,
				'first_options' => [
					'label' => 'Password',
				],
				'second_options' => [
					'label' => 'Repeat Password'
				]
			])
			->add('passwordChangeDate', HiddenType::class, [
				'data' => $now->format(DATE_ATOM)
			])
			->add('passwordChangeNeeded', HiddenType::class, [
				'data' => 0
			])
			->add('changePassword' , SubmitType::class)

		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'AppBundle\Entity\UserEntity'
		]);
	}



}