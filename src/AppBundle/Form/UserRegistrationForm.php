<?php

namespace AppBundle\Form;

use AppBundle\Entity\UserEntity;
use Doctrine\DBAL\Types\StringType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegistrationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
			->add('uid', TextType::class, [
				'required' => true,
				'attr' => array('autocomplete' => 'email'),
				'label' => 'Email Address',
				'label_attr' =>array('class' => 'formRequiredLabel')
			])
			->add('plainPassword', RepeatedType::class, [
				'type' => PasswordType::class,
				'attr' => array('autocomplete' => 'off'),
				'required' => true
			])
			->add('name',TextType::class, [
				'attr' => array('autocomplete' => 'name'),
				'label' => 'Full Name',
				'required' => true,

			])
			->add('address',TextType::class, [
				'attr' => array('autocomplete' => 'address-line1'),
				'label' => 'Address',
				'required' => false
			])
			->add('city',TextType::class, [
				'attr' => array('autocomplete' => 'city'),
				'label' => 'City',
				'required' => false
			])
			->add('state', EntityType::class, [
				'class' => 'AppBundle\Entity\StatesEntity',
				'choice_label' => 'name',
				'required' =>false,
				'attr' => array('autocomplete' => 'state'),
				'label' => 'State'
			])
			->add('zip',TextType::class, [
				'attr' => array('autocomplete' => 'postal-code'),
				'label' => 'Postal Code',
				'required' => false
			])
			->add('phone',TextType::class, [
				'attr' => array('autocomplete' => 'mobile'),
				'label' => 'Phone Number'
			])
			->add('comment', TextareaType::class, [
				'attr' => array('autocomplete' => 'off', 'maxlength' => '255'),
				'label' => 'System IDs / Comments',
				'required' => false
			])
			->add('email', HiddenType::class, [
				'attr' => array('class' => 'js-registerEmailFind')
			])
			;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
		$resolver->setDefaults([
			'data_class' => UserEntity::class,
			'validation_groups' => ['Default', 'Registration']
		]);
    }

}
