<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/29/2017
 * Time: 10:27 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class StatesEntity
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatesRepository")
 * @ORM\Table(name="misc_states")
 */
class StatesEntity
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\Column(type="string", length=10)
	 */
	private $abv;

	/**
	 * @ORM\Column(type="string", length=50)
	 */
	private $name;

	/**
	 * @return string
	 */
	public function getAbv()
	{
		return $this->abv;
	}

	/**
	 * @param string $abv
	 */
	public function setAbv($abv)
	{
		$this->abv = $abv;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	public function __toString()
	{
		return strval($this->getAbv());
	}

}//end class