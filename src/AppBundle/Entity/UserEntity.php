<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 12/20/2016
 * Time: 11:55 AM
 */

namespace AppBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Rollerworks\Component\PasswordStrength\Validator\Constraints as RollerworksPassword;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(fields={"uid"}, message="It looks like you already have an account!")

 */
class UserEntity implements UserInterface
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string", unique=true)
	 */
	private $uid;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $enabled = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $registered = false;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string")
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $address;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $city;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $state;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $zip;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string")
	 */
	private $phone;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Email(
	 *     message = "'{{ value }}' is not a valid email.",
	 *     checkMX = true
	 * )
	 * @ORM\Column(type="string")
	 */
	private $email;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $comment;


	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $password;

	/**
	 * @ORM\Column(type="json_array")
	 */
	private $roles = [];

	/**
	 * @ORM\Column(type="boolean", options={"default":1})
	 */
	private $passwordChangeNeeded = true;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $passwordChangeDate;

	/**
	 * A non-persisted field that's used to create the encoded password.
	 * @Assert\NotBlank(groups={"Registration"})
	 * @var string
	 * @RollerworksPassword\PasswordStrength(minLength=7, minStrength=3)
	 */
	private $plainPassword;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string")
	 */
	private $timeZone;

	/**
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Returns the username used to authenticate the user.
	 *
	 * @return string The username
	 */
	public function getUsername()
	{
		return $this->uid;
	}

	/**
	 * Returns the roles granted to the user.
	 *
	 * <code>
	 * public function getRoles()
	 * {
	 *     return array('ROLE_USER');
	 * }
	 * </code>
	 *
	 * Alternatively, the roles might be stored on a ``roles`` property,
	 * and populated in any number of different ways when the user object
	 * is created.
	 *
	 * @return (Role|string)[] The user roles
	 */
	public function getRoles()
	{
		$roles = $this->roles;
		if(!in_array('ROLE_USER', $roles)){
			$roles[] = 'ROLE_USER';
		}
		return $roles;
	}

	/**
	 * @param mixed $roles
	 */
	public function setRoles(array $roles)
	{
		if(!in_array('ROLE_USER', $roles)){
			$roles[] = 'ROLE_USER';
		}
		$this->roles =$roles;

	}

	/**
	 * Returns the password used to authenticate the user.
	 *
	 * This should be the encoded password. On authentication, a plain-text
	 * password will be salted, encoded, and then compared to this value.
	 *
	 * @return string The password
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param mixed $password
	 */
	public function setPassword($password)
	{
		$this->password = $password;
	}

	/**
	 * @return mixed
	 */
	public function getPlainPassword()
	{
		return $this->plainPassword;
	}

	/**
	 * @param mixed $plainPassword
	 */
	public function setPlainPassword($plainPassword)
	{
		$this->plainPassword = $plainPassword;
		// forces the object to look "dirty" to Doctrine. Avoids
		// Doctrine *not* saving this entity, if only plainPassword changes
		$this->password = null;
	}



	/**
	 * Returns the salt that was originally used to encode the password.
	 *
	 * This can return null if the password was not encoded using a salt.
	 *
	 * @return string|null The salt
	 */
	public function getSalt()
	{
	}

	/**
	 * Removes sensitive data from the user.
	 *
	 * This is important if, at any given point, sensitive information like
	 * the plain-text password is stored on this object.
	 */
	public function eraseCredentials()
	{
		$this->plainPassword = null;
	}

	/**
	 * @param mixed $uid
	 */
	public function setUid($uid)
	{
		$this->uid = $uid;
	}

	/**
	 * @return mixed
	 */
	public function getUid()
	{
		return $this->uid;
	}

	/**
	 * @return bool
	 */
	public function getEnabled()
	{
		return $this->enabled;
	}

	/**
	 * @param bool $enabled
	 */
	public function setEnabled($enabled)
	{
		$this->enabled = $enabled;
	}

	/**
	 * @return bool
	 */
	public function getRegistered()
	{
		return $this->registered;
	}

	/**
	 * @param bool $registered
	 */
	public function setRegistered($registered)
	{
		$this->registered = $registered;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * @param string $address
	 */
	public function setAddress($address)
	{
		$this->address = $address;
	}

	/**
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity($city)
	{
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getState()
	{
		return $this->state;
	}

	/**
	 * @param string $state
	 */
	public function setState($state)
	{
		$this->state = $state;
	}

	/**
	 * @return string
	 */
	public function getZip()
	{
		return $this->zip;
	}

	/**
	 * @param string $zip
	 */
	public function setZip($zip)
	{
		$this->zip = $zip;
	}

	/**
	 * @return string
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 * @param string $comment
	 */
	public function setComment($comment)
	{
		$this->comment = $comment;
	}

	/**
	 * @return mixed
	 */
	public function getPasswordChangeNeeded()
	{
		return $this->passwordChangeNeeded;
	}

	/**
	 * @param mixed $passwordChangeNeeded
	 */
	public function setPasswordChangeNeeded($passwordChangeNeeded)
	{
		$this->passwordChangeNeeded = $passwordChangeNeeded;
	}

	/**
	 * @return mixed
	 */
	public function getPasswordChangeDate()
	{
		return $this->passwordChangeDate;
	}

	/**
	 * @param mixed $passwordChangeDate
	 */
	public function setPasswordChangeDate($passwordChangeDate)
	{
		$this->passwordChangeDate = $passwordChangeDate;
	}

	/**
	 * @return mixed
	 */
	public function getTimeZone()
	{
		return $this->timeZone;
	}

	/**
	 * @param mixed $timeZone
	 */
	public function setTimeZone($timeZone)
	{
		$this->timeZone = $timeZone;
	}




}