<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 3:48 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class StatesEntity
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DictWordsRepository")
 * @ORM\Table(name="misc_dictwords")
 */
class DictWordsEntity
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=10)
	 */
	private $word;

	/**
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param integer $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getWord()
	{
		return $this->word;
	}

	/**
	 * @param string $word
	 */
	public function setWord($word)
	{
		$this->word = $word;
	}
}