<?php

namespace AppBundle\Menu;


use AppBundle\Entity\SystemsDataGeEntity;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\User\User;


class Builder implements ContainerAwareInterface
{
	use ContainerAwareTrait;

	public function mainMenu(FactoryInterface $factory, array $options)
	{

		$logged_in = false;
		if( isset($options['user']) and $options['user'] != null){
			$logged_in = true;
		}

		// Menu will be a navbar menu anchored to right
		$menu = $factory->createItem('root');
		$menu->setChildrenAttribute('class', 'nav pull-right');

		if($logged_in){

			// Add a regular child with a badge and icon, icon- is prepended automatically
			// If you pass false to badge it will not show
			// This can be useful for 'badge' => $badgeData,  where $badgeData is a DB result (message or notification counts)
			// badge-class can be used to style the badge



			$home = $menu->addChild('Home', array(
				'icon' => 'home',
				'badge-class' => 'menu-badge',
				'route' => 'homepage',
			));



		}else{
			$menu->addChild('', array(
				'icon' => ' glyphicon glyphicon-lock',
				'route' => 'security_login',
			));

//			$menu->addChild('Register', array(
//				'icon' => ' glyphicon glyphicon-thumbs-up',
//				'route' => 'user_register',
//			));

		}



		return $menu;
	}
	public function userMenu(FactoryInterface $factory, array $options)
	{
		// Menu will be a navbar menu anchored to right
		$menu = $factory->createItem('root', array(
			'navbar ' => true,
			'navbar-right' => true,
		));

		// Create a dropdown with a caret
		$dropdown = $menu->addChild('User', array(
			'dropdown' => true,
			'caret' => true,
		));

		// Create a dropdown header
		$dropdown->addChild('Username: '.$options['user']->getUsername(), array('dropdown-header' => true));
		$dropdown->addChild('Profile', array('route' => 'users_profile'));
		$dropdown->addChild('Preferences', array('route' => 'users_prefs'));

//		$layout = $menu->addChild('Logout', array(
//			'icon' => ' glyphicon glyphicon-off',
//			'route' => 'security_logout',
//		));


		return $menu;
	}

	public function adminMenu(FactoryInterface $factory, array $options = null)
	{
		// Menu will be a navbar menu anchored to right
		$menu = $factory->createItem('root', array(
			'navbar ' => true,
			'navbar-right' => true,
		));

		// Create a dropdown with a caret
		$dropdown = $menu->addChild('Admin', array(
			'dropdown' => true,
			'caret' => true,
		));

		// Create a dropdown header
		$dropdown->addChild('Dashboard', array('route' => 'adminHomepage'));
		$dropdown->addChild('divider_1', array('divider' => true));
		$dropdown->addChild('Manage Users', array('route' => 'users_view'));
		$dropdown->addChild('Add New User', array('route' => 'users_new'));
		$dropdown->addChild('divider_4', array('divider' => true));
		$dropdown->addChild('Logs', array('route' => 'under_construction'));

		return $menu;
	}

	public function logoutMenu(FactoryInterface $factory, array $options = null)
	{
		// Menu will be a navbar menu anchored to right
		$menu = $factory->createItem('root', array(
			'navbar ' => true,
			'navbar-right' => true,
		));

		$layout = $menu->addChild('Logout', array(
			'icon' => ' glyphicon glyphicon-off',
			'route' => 'security_logout',
		));

		return $menu;
	}
}