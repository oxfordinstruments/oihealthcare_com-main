<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 12/20/2016
 * Time: 11:23 PM
 */

namespace AppBundle\Doctrine;

use AppBundle\Entity\UserEntity;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class HashPasswordListener implements EventSubscriber
{
	/**
	 * @var UserPasswordEncoder
	 */
	private $passwordEncoder;


	/**
	 * HashPasswordListener constructor.
	 * @param UserPasswordEncoder $passwordEncoder
	 */
	public function __construct(UserPasswordEncoder $passwordEncoder)
	{
		$this->passwordEncoder = $passwordEncoder;
	}

	/**
	 * @param LifecycleEventArgs $args
	 * @return null
	 */
	public function prePersist(LifecycleEventArgs $args)
	{
		$entity = $args->getEntity();
		if(!$entity instanceof UserEntity){
			return null;
		}
//		dump($entity);die();
		$this->encodePassword($entity);

	}

	/**
	 * @param LifecycleEventArgs $args
	 * @return null
	 */
	public function preUpdate(LifecycleEventArgs $args)
	{
		$entity = $args->getEntity();
		if(!$entity instanceof UserEntity){
			return null;
		}
//		dump($entity);
		$this->encodePassword($entity);
//		dump($entity); die();
		$em = $args->getEntityManager();
		$meta = $em->getClassMetadata(get_class($entity));
		$em->getUnitOfWork()->recomputeSingleEntityChangeSet($meta, $entity);

	}

	/**
	 * Returns an array of events this subscriber wants to listen to.
	 *
	 * @return array
	 */
	public function getSubscribedEvents()
	{
		return ['prePersist', 'preUpdate'];
	}

	/**
	 * @param UserEntity $entity
	 */
	public function encodePassword(UserEntity $entity)
	{
		if (!$entity->getPlainPassword()) {
			return;
		}

		$encoded = $this->passwordEncoder->encodePassword($entity, $entity->getPlainPassword());
		$entity->setPassword($encoded);
	}
}