<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/29/2017
 * Time: 4:05 PM
 */

namespace AppBundle\Repository;

use AppBundle\Entity\DictWordsEntity;
use Doctrine\ORM\EntityRepository;

class DictWordsRepository extends EntityRepository
{
	public function findRandWords($count)
	{
		$em = $this->getEntityManager();
		return $em->createQuery(
			'SELECT word FROM AppBundle:DictWordsEntity AS d ORDER BY RAND() LIMIT 0,3; '
		)->getResult();
	}

	public function findRandWordsNew($count)
	{
		$em = $this->getEntityManager();
		$words = $em->getRepository(DictWordsEntity::class)->findAll();
		shuffle($words);
		return array_slice($words, 0 , $count);
	}
}