var gulp = require('gulp');

var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var cleanCss = require('gulp-clean-css');
var util = require('gulp-util');
var gulpif = require('gulp-if');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var del = require('del');
var rev = require('gulp-rev');
var Q = require('q');

//var plugins = require('gulp-load-plugins')({lazy:false});

var config = {
	varDir: 'var',
	assetsDir: 'app/Resources/assets',
	stylesPattern: 'styles/**/*.scss',
	scriptsPattern: 'js/**/*.js',
	// bowerDir: 'vendor/bower_components',
	nodeDir: 'node_modules',
	production: !!util.env.production,
	sourceMaps: !util.env.production
};

var tasks;
if(config.production){
	tasks = ['clean', 'styles', 'scripts', 'fonts', 'images'];
}else{
	tasks = ['clean', 'styles', 'scripts', 'fonts', 'images', 'watch'];
}
gulp.task('default', tasks);

var app = {};

app.addStyle = function (paths, filename) {
	return gulp.src(paths)
		.pipe(plumber(function (error) {
			console.log(error.toString());
			this.emit('end');
		}))
		.pipe(gulpif(config.sourceMaps,sourcemaps.init()))
		.pipe(sass())
		.pipe(concat('css/'+filename))
		.pipe(config.production ? cleanCss() : util.noop())
		.pipe(rev())
		.pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
		.pipe(gulp.dest('web'))
		.pipe(rev.manifest(config.varDir+'/rev-manifest.json',{merge:true}))
		.pipe(gulp.dest('.'));
};

app.addScripts = function (paths, filename) {
	return gulp.src(paths)
		.pipe(plumber(function (error) {
			console.log(error.toString());
			this.emit('end');
		}))
		.pipe(gulpif(config.sourceMaps,sourcemaps.init()))
		.pipe(concat('js/'+filename))
		.pipe(config.production ? uglify() : util.noop())
		.pipe(rev())
		.pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
		.pipe(gulp.dest('web'))
		.pipe(rev.manifest(config.varDir+'/rev-manifest.json',{merge:true}))
		.pipe(gulp.dest('.'));
};

app.copy = function (srcFiles, outputDir) {
	return gulp.src(srcFiles)
		.pipe(gulp.dest(outputDir));
};

app.del = function (dir) {
	return del.sync(dir);
};

var Pipeline = function() {
	this.entries = [];
};

Pipeline.prototype.add = function() {
	this.entries.push(arguments);
};

Pipeline.prototype.run = function(callable) {
	var deferred = Q.defer();
	var i = 0;
	var entries = this.entries;
	var runNextEntry = function() {
		// see if we're all done looping
		if (typeof entries[i] === 'undefined') {
			deferred.resolve();
			return;
		}
		// pass app as this, though we should avoid using "this"
		// in those functions anyways
		callable.apply(app, entries[i]).on('end', function() {
			i++;
			runNextEntry();
		});
	};
	runNextEntry();
	return deferred.promise;
};

gulp.task('clean', function () {
	app.del(
		config.varDir+'/rev-manifest.json',
		'web/css/*',
		'web/js/*',
		'web/fonts/*'
	);
});

gulp.task('styles', function () {
	var pipeline = new Pipeline();
	app.del('web/css/*');

	pipeline.add([
		config.nodeDir+'/bootstrap/dist/css/bootstrap.min.css'
	],'bootstrap.css');

	pipeline.add([
		config.nodeDir+'/font-awesome/css/font-awesome.min.css'
	],'font-awesome.css');

	pipeline.add([
		config.assetsDir+'/styles/layout.scss',
		config.assetsDir+'/styles/styles.scss'
	],'main.css');

	pipeline.add([
		config.assetsDir+'/styles/layout-mobile.scss',
		config.assetsDir+'/styles/styles-mobile.scss'
	],'main-mobile.css');

	pipeline.add([
		config.assetsDir+'/styles/adm.scss'
	],'adm.css');

	pipeline.add([
		config.assetsDir+'/styles/home.scss'
	],'home.css');

	pipeline.add([
		config.assetsDir+'/styles/singlePage.scss'
	], 'single.css');

	return pipeline.run(app.addStyle);

});

gulp.task('scripts', function () {
	var pipeline = new Pipeline();

	app.del('web/js/*');

	pipeline.add([
		config.assetsDir+'/js/main.js'
	],'site.js');

	pipeline.add([
		config.assetsDir+'/js/adm.js'
	],'adm.js');

	pipeline.add([
		config.assetsDir+'/js/users.js'
	],'users.js');

	pipeline.add([
		config.assetsDir+'/js/home.js'
	],'home.js');

	pipeline.add([
		config.nodeDir+'/jquery/dist/jquery.min.js'
	],'jquery.js');

	pipeline.add([
		config.nodeDir+'/bootstrap/dist/js/bootstrap.min.js'
	],'bootstrap.js');


	return pipeline.run(app.addScripts);
});

gulp.task('fonts', function () {
	app.copy(
		config.nodeDir+'/font-awesome/fonts/*',
		'web/fonts'
	);
	app.copy(
		config.nodeDir+'/bootstrap/dist/fonts/*',
		'web/fonts'
	);

});

gulp.task('images', function () {
	return app.copy(
		config.assetsDir+'/images/**/*',
		'web/images'
	);
});

gulp.task('watch', function () {
	gulp.watch(config.assetsDir+'/'+config.stylesPattern, ['styles']);
	gulp.watch(config.assetsDir+'/'+config.scriptsPattern, ['scripts']);
});