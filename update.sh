#!/usr/bin/env bash
php composer.phar update

if [ $? -eq 0 ]
then
  echo "Composer Update Successful"
else
  echo "Composer Update Failed" >&2
  echo "Hit any key to continue..."
  read NONE
  exit 1
fi

php composer.phar install

if [ $? -eq 0 ]
then
  echo "Composer Install Successful"
else
  echo "Composer Install Failed" >&2
  echo "Hit any key to continue..."
  read NONE
  exit 1
fi

npm update

if [ $? -eq 0 ]
then
  echo "NPM Install/Update Successful"
else
  echo "NPM Install/Update Failed" >&2
  echo "Hit any key to continue..."
  read NONE
  exit 1
fi

echo "Hit any key to continue..."
read NONE
