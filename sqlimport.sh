#!/usr/bin/env bash
#leaving the commented out parts for reference.

#phpcmd=(php -f ./web/doccase.php $1)
#tbl=$("${phpcmd[@]}")

#rm ./src/AppBundle/Entity/Mac*
#rm ./src/AppBundle/Resources/config/doctrine/*
#
#echo $1
#echo $2
#echo $3
#echo "Hit any key to continue..."
#read NONE
#exit 0

retVal=0
php $2/bin/console doctrine:mapping:import --force AppBundle annotation --filter=$1
if [ ! $? -eq 0 ]; then
    echo "Import Error"
    exit 1
fi
php $2/bin/console doctrine:mapping:convert xml ../src/AppBundle/Entity/Mac --from-database --force --filter=$1
if [ ! $? -eq 0 ]; then
    echo "Convert Error"
    exit 2
fi
php $2/bin/console doctrine:generate:entities AppBundle:$1 --no-backup -v
if [ ! $? -eq 0 ]; then
    echo "Generate Error"
    exit 3
fi

echo $retVal
#echo "Hit any key to continue..."
#read NONE

exit $retVal