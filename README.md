OiHealthcare Main Site
=========

*A Symfony 3 project created by Evotodi.*


###Production Tasks:
~~~~
composer install
npm install
npm install -g gulp
gulp gulpfile.js --production
bin/console seed:load
bin/console cache:clear --env=prod
~~~~

~~~~
clear the prod env cache
~~~~

<br>
<br>

###Thanks To:
[KNP University](https://knpuniversity.com/)